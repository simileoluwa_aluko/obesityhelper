package aluko.simileoluwa.obesityhelper

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar

import kotlinx.android.synthetic.main.activity_result.*

class ResultActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        setListeners()

        val height = intent.getIntExtra("Height", 0)
        val heightUnit = intent.getStringExtra("HeightUnit")
        Toast.makeText(this, "$height" + heightUnit , Toast.LENGTH_LONG).show()
    }

    private fun setListeners(){
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }
    }

}
