package aluko.simileoluwa.obesityhelper.Calculations

class WaistHIpRatio(val waistSize : Int, val hipSize : Int) {

    fun getWaistHipRatio(): Int {
        val waistHipRatio = waistSize / hipSize
        return waistHipRatio
    }
}