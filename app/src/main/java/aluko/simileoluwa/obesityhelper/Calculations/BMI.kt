package aluko.simileoluwa.obesityhelper.Calculations

class BMI(val weight : Int, val height : Int) {

    fun getBMI() : Int{
        val bmi = weight/(height * height)
        return bmi
    }
}