package aluko.simileoluwa.obesityhelper.Calculations

class BFF(private val waistSize : Int, private val wristSize : Int, private val weight : Int, private val gender : GenderEnum, private val hipSize : Int,
          private val forearmLength : Int) {

    private fun getBFF(): Double {
        var bff = 0.0
        if(gender == GenderEnum.FEMALE) bff = getFemaleBFF(weight, wristSize, waistSize, hipSize, forearmLength)
        else if (gender == GenderEnum.MALE) bff = getMaleBFF(weight, waistSize)
        return bff
    }

    private fun getFemaleBFF(weight: Int, wristSize: Int, waistSize: Int, hipSize: Int, forearmLength: Int): Double {
        val weightFactor = (weight * 0.732)  + 8.987
        val wristFactor   = wristSize/3.140
        val waistSizeFactor = waistSize * 0.157
        val hipSizeFactor = hipSize * 0.249
        val forearmLengthFactor = forearmLength * 0.434
        val leanBodyMass = weightFactor + wristFactor - waistSizeFactor - hipSizeFactor + forearmLengthFactor
        val bodyFatWeight = weight - leanBodyMass
        val bodyFatPercentage = (bodyFatWeight * 100) / weight

        return bodyFatPercentage
    }
    private fun getMaleBFF(weight: Int, waistSize: Int): Double {
        val weightFactor = (weight * 1.082) + 94.42
        val waistFactor = waistSize * 4.15
        val leanBodyMass = weightFactor - waistFactor
        val bodyFatWeight = weight - leanBodyMass
        val bodyFatPercentage = (bodyFatWeight * 100) / weight

        return bodyFatPercentage
    }
}
