package aluko.simileoluwa.obesityhelper.Calculations


class BMR(val height : Int, private val weight : Int, private val age : Int, private val gender : GenderEnum) {

    fun getBMR(): Double {
        var bmr : Double = 0.0
        if(gender == GenderEnum.MALE) bmr = getMaleBMR(height, weight, age)
        else if (gender == GenderEnum.FEMALE) bmr = getFemaleBMR(height, weight, age)
        return bmr
    }

    private fun getFemaleBMR(height : Int, weight: Int, age: Int): Double = 655 + (9.6 * weight) + (1.8 * height) - (4.7 * age)


    private fun getMaleBMR(height : Int, weight: Int, age: Int) : Double = 66 + (13.75 * weight) + (5 * height) - (6.8 * age)


}

enum class GenderEnum {MALE, FEMALE}

