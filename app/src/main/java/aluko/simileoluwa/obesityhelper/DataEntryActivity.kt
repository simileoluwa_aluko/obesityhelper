package aluko.simileoluwa.obesityhelper

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputEditText
import kotlinx.android.synthetic.main.activity_data_entry.*

class DataEntryActivity : AppCompatActivity() {

    private var height = 0
    private var weight = 0
    private var wristSize = 0
    private var waistSize = 0
    private var hipSize = 0
    private var forearmLength = 0
    private var age = 0

    private lateinit var heightUnit : String
    private lateinit var weightUnit : String
    private lateinit var wristSizeUnit : String
    private lateinit var waistSizeUnit : String
    private lateinit var hipSizeUnit : String
    private lateinit var forearmLengthUnit : String
    private lateinit var gender: String

    private lateinit var heightEditText : TextInputEditText
    private lateinit var weightEditText : TextInputEditText
    private lateinit var wristSizeEditText : TextInputEditText
    private lateinit var waistEditText : TextInputEditText
    private lateinit var hipEditText : TextInputEditText
    private lateinit var forearmEditText : TextInputEditText
    private lateinit var ageEditText : TextInputEditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_entry)

        heightEditText = findViewById<TextInputEditText>(R.id.height)
        weightEditText = findViewById<TextInputEditText>(R.id.weight)
        wristSizeEditText = findViewById<TextInputEditText>(R.id.wrist_size)
        waistEditText = findViewById<TextInputEditText>(R.id.waist_size)
        hipEditText = findViewById<TextInputEditText>(R.id.hip_size)
        forearmEditText = findViewById<TextInputEditText>(R.id.forearm_lenght)
        ageEditText = findViewById<TextInputEditText>(R.id.age)

        setAdapters()
        setClickListeners()
    }

    private fun setClickListeners() {

        body_measurement_continue_btn.setOnClickListener { _ ->
            val editTextArray = arrayOf(heightEditText, weightEditText, wristSizeEditText, waistEditText, hipEditText,
                    forearmEditText, ageEditText)

            val isAnyEmpty = validateEmptyEditText(editTextArray)

            if (isAnyEmpty) {
                Toast.makeText(this@DataEntryActivity, "No field should be empty", Toast.LENGTH_SHORT).show()
            } else {
                height = Integer.parseInt(heightEditText.text.toString())
                weight = Integer.parseInt(weightEditText.text.toString())
                wristSize = Integer.parseInt(wristSizeEditText.text.toString())
                waistSize = Integer.parseInt(waistEditText.text.toString())
                hipSize = Integer.parseInt(hipEditText.text.toString())
                forearmLength = Integer.parseInt(forearmEditText.text.toString())
                age = Integer.parseInt(ageEditText.text.toString())

                gender = gender_spinner.selectedItem.toString()
                heightUnit = height_spinner.selectedItem.toString()
                weightUnit = weight_spinner.selectedItem.toString()
                wristSizeUnit = wrist_size_spinner.selectedItem.toString()
                hipSizeUnit = hip_size_spinner.selectedItem.toString()
                forearmLengthUnit = forearm_length_spinner.selectedItem.toString()
                waistSizeUnit = waist_size_spinner.selectedItem.toString()

                val intent = Intent(this, ResultActivity::class.java)
                intent.apply {
                    putExtra("Height", height)
                    putExtra("Weight", weight)
                    putExtra("WaistSize", waistSize)
                    putExtra("WristSize", wristSize)
                    putExtra("HipSize", hipSize)
                    putExtra("Age", age)
                    putExtra("Gender", gender)
                    putExtra("ForearmLength", forearmLength)

                    putExtra("HeightUnit", heightUnit)
                    putExtra("WeightUnit", weightUnit)
                    putExtra("WaistUnit", waistSizeUnit)
                    putExtra("WristUnit", wristSizeUnit)
                    putExtra("HipSize", hipSizeUnit)
                    putExtra("ForearmUnit", forearmLengthUnit)
                }

                startActivity(intent)
            }

        }

    }

    private fun validateEmptyEditText(editTexts: Array<TextInputEditText>): Boolean {

        var isAnyEmpty = false

        editTexts.forEach {
            if (it.text?.isEmpty()!!) {
                isAnyEmpty = true
            }
        }

        return isAnyEmpty
    }

    private fun setAdapters() {
        ArrayAdapter.createFromResource(this, R.array.weight_units, android.R.layout.simple_spinner_item)
                .also { adapter ->
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    weight_spinner.adapter = adapter
                }

        ArrayAdapter.createFromResource(this, R.array.linear_units, android.R.layout.simple_spinner_item)
                .also { adapter ->
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    height_spinner.adapter = adapter
                    waist_size_spinner.adapter = adapter
                    hip_size_spinner.adapter = adapter
                    wrist_size_spinner.adapter = adapter
                    forearm_length_spinner.adapter = adapter
                }
        ArrayAdapter.createFromResource(this, R.array.gender, android.R.layout.simple_spinner_item)
                .also { adapter ->
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    gender_spinner.adapter = adapter
                }
    }
}